#!/bin/bash

## Help ##
Help()
{
    # Displays help for the script
    echo
    echo " Kitty local theme changer!"
    echo
    echo " Syntax: kitty-theme.sh [-l|-h|-v]"
    echo " options:"
    echo "       -l    Lists all the local theme confs!!"
    echo "       -h    Prints this help text!!"
    echo "       -v    Shows version number!!"
    echo
}

## Version ##
Version()
{
    # Displays version number of the script!!
    echo "v0.1"
}

## Listing available theme confs ##
List()
{
    for entry in $XDG_CONFIG_HOME/kitty/themes/*; do
        echo "${entry##*/}"
    done
}

## Main function of the script!!
# Theme changing
#Theme()
#{
#
#}

## Options ##
# Get options from stdin
while getopts "hvlt" option; do
    case $option in
        h) # Displays help text
            Help
            exit;;
        v) # Displays version
            Version
            exit;;
        l) # Lists themes in themes folder $XDG_CONFIG_HOME/kitty/themes
            List
            exit;;
        t) # Selecting theme
            Theme
            exit;;
    esac
done
