#!/bin/bash

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar date &
polybar mid &
polybar right &
polybar player &
polybar window &

echo "Polybar started..."
