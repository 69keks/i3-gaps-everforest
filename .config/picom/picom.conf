#################################
#          Animations           #
#################################
# requires https://github.com/jonaburg/picom
# (These are also the default values)
transition-length = 300
transition-pow-x = 0.5
transition-pow-y = 0.5
transition-pow-w = 0.1
transition-pow-h = 0.5
size-transition = true


#################################
#             Corners           #
#################################
# requires: https://github.com/sdhand/compton or https://github.com/jonaburg/picom
corner-radius = 5.0;
rounded-corners-exclude = [
  #"window_type = 'normal'",
  "class_g = 'Polybar'",
  "class_g = 'Rofi'",
  "class_g = 'Eww daemon'",
  "class_g = 'Tint2'"
];
round-borders = 0;
round-borders-rule = [
  "3:class_g *?= ''"
];

#################################
#             Shadows           #
#################################


# Enabled client-side shadows on windows. Note desktop windows 
# (windows with '_NET_WM_WINDOW_TYPE_DESKTOP') never get shadow, 
# unless explicitly requested using the wintypes option.
#
# shadow = false
shadow = true;

# The blur radius for shadows, in pixels. (defaults to 12)
# shadow-radius = 12
shadow-radius = 17;

# The opacity of shadows. (0.0 - 1.0, defaults to 0.75)
shadow-opacity = 0.5

# shadow-exclude = []
shadow-exclude = [
  "class_g = 'Polybar'"
];

#################################
#           Fading              #
#################################


# Fade windows in/out when opening/closing and when opacity changes,
#  unless no-fading-openclose is used.
# fading = false
fading = true;

# Opacity change between steps while fading in. (0.01 - 1.0, defaults to 0.028)
# fade-in-step = 0.028
fade-in-step = 0.03;

# Opacity change between steps while fading out. (0.01 - 1.0, defaults to 0.03)
# fade-out-step = 0.03
fade-out-step = 0.03;

#################################
#   Transparency / Opacity      #
#################################

# Opacity of window titlebars and borders. (0.1 - 1.0, disabled by default)
# frame-opacity = 1.0
frame-opacity = 1.0;

# Specify a list of conditions of windows that should always be considered focused.
# focus-exclude = []
focus-exclude = [
  "class_g = 'Cairo-clock'",
  "class_g = 'Bar'",                    # lemonbar
  "class_g = 'eww'",
  "class_g = 'slop'"                    # maim
];

#################################
#     Background-Blurring       #
#################################

blur: {
  # requires: https://github.com/ibhagwan/picom
  method = "dual_kawase";
  #method = "kernel";
  strength = 8;
  # deviation = 1.0;
  # kernel = "11x11gaussian";
  background = false;
  background-frame = false;
  background-fixed = false;
}

#################################
#       General Settings        #
#################################

# Specify the backend to use: `xrender`, `glx`, or `xr_glx_hybrid`.
# `xrender` is the default one.

experimental-backends = true;
backend = "glx";
#backend = "xrender";


# Enable/disable VSync.
# vsync = false
vsync = false

# Try to detect WM windows (a non-override-redirect window with no 
# child that has 'WM_STATE') and mark them as active.
#
# mark-wmwin-focused = false
mark-wmwin-focused = true;

# Mark override-redirect windows that doesn't have a child window with 'WM_STATE' focused.
# mark-ovredir-focused = false
mark-ovredir-focused = true;

# Try to detect windows with rounded corners and don't consider them 
# shaped windows. The accuracy is not very high, unfortunately.
#
# detect-rounded-corners = false
detect-rounded-corners = true;

# Detect '_NET_WM_OPACITY' on client windows, useful for window managers
# not passing '_NET_WM_OPACITY' of client windows to frame windows.
#
# detect-client-opacity = false
detect-client-opacity = true;

# Specify refresh rate of the screen. If not specified or 0, picom will 
# try detecting this with X RandR extension.
#
# refresh-rate = 60
refresh-rate = 0

# Use 'WM_TRANSIENT_FOR' to group windows, and consider windows 
# in the same group focused at the same time.
#
# detect-transient = false
detect-transient = true

# Use 'WM_CLIENT_LEADER' to group windows, and consider windows in the same 
# group focused at the same time. 'WM_TRANSIENT_FOR' has higher priority if 
# detect-transient is enabled, too.
#
# detect-client-leader = false
detect-client-leader = true

# Disable the use of damage information. 
# This cause the whole screen to be redrawn everytime, instead of the part of the screen
# has actually changed. Potentially degrades the performance, but might fix some artifacts.
# The opposing option is use-damage
#
# no-use-damage = false
#use-damage = true (Causing Weird Black semi opaque rectangles when terminal is opened)
#Changing use-damage to false fixes the problem
use-damage = false

wintypes:
{
  normal = { fade = true; shadow = true; }
  tooltip = { fade = true; shadow = true; opacity = 0.75; focus = true; full-shadow = false; };
  dock = { shadow = false; }
  dnd = { shadow = false; }
  popup_menu = { opacity = 0.8; }
  dropdown_menu = { opacity = 0.8; }
};
