#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='exa --icons --group-directories-first'
alias l='ls -alF --no-time'

alias vi='nvim'
alias vim='nvim'

export EDITOR='nvim'
export BAT_THEME='base16'

PS1='[\u@\h \W]\$ '

# Ready for takeoff 🚀 !!
#eval "$(starship init bash)"
